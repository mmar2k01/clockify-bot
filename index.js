const { Telegraf, Markup } = require("telegraf");
const bot = new Telegraf("1213198009:AAFXTywxI0FVt7TfTE-qXwSYeedfIwjUbrY");
const dbctx = require("./dbContext");
const control = require("./controller");

bot.launch();

bot.start((ctx) => {
  control.calculationStart(ctx);
});

bot.on("callback_query", (ctx) => {
    control.calculationCallbackQuery(ctx);
});

bot.on("text", (ctx) => {
  control.calculationText(ctx);
  if(ctx.message.text.includes('/delete')){
    control.calculationCammandDeleteEntry(ctx);
  }
});
