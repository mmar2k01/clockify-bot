const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  chatid: String,
  state: String,
  token: String,
  workid: String,
  email: String,
  messageid: String,
});
const userModel = new mongoose.model("person", userSchema);
module.exports = { userModel };
