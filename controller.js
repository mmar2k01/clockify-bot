const { Telegraf, Markup } = require("telegraf");
const bot = new Telegraf("1213198009:AAFXTywxI0FVt7TfTE-qXwSYeedfIwjUbrY");
const dbctx = require("./dbContext");
const view = require("./view");
const api = require("./api");
const db = require("./db");
async function calculationStart(ctx) {
  if ((await dbctx.is(ctx.from.id)) == true) {
    dbctx.add(ctx.from.id, "started");
    view.wellcome(ctx);
  } else {
    dbctx.deleteUser(ctx.from.id);
    dbctx.add(ctx.from.id, "started");
    view.wellcome(ctx);
  }
}

async function calculationCallbackQuery(ctx) {
  if (ctx.update.callback_query.data == "SalaryThisYear"){
   let salari=await api.salaryThisYear(ctx.from.id);
   await view.salariReport(ctx,salari.totalTime,salari.totalBillableTime,salari.entriesCount);
   ctx.deleteMessage();
  }
  if (ctx.update.callback_query.data == "back") {
    let getToken = await dbctx.get(ctx.from.id);
    let workSpace = await api.getWorkSpaces(getToken.token);
    let strId = "",
      strName = "";
    workSpace.data.forEach((element) => {
      strName += element.name + ",";
      strId += element.id + ",";
    });
    view.okTokenAndGetWorkSpaces(ctx, strId, strName);
  }
  let getToken = await dbctx.get(ctx.from.id);
  let getWorkSpaces = await api.getWorkSpaces(getToken.token);
  let workSpaces = "";
  getWorkSpaces.data.forEach((element) => {
    workSpaces += element.id;
  });
  if (workSpaces.includes(ctx.update.callback_query.data)) {
    let message = "";
    let times = await api.getEntry(
      ctx.update.callback_query.data,
      await api.getUserId(ctx.update.callback_query.data, getToken.token),
      getToken.token
    );
    times.data.forEach((element) => {
      let duration = element.timeInterval.duration;
      message +=
        element.description +
        "\n" +
        "time :" +
        duration +
        "\n" +
        "/delete_" +
        element.id +
        "\n";
    });
    view.sendMessageWithReport(ctx, message);
    ctx.deleteMessage();
    dbctx.updateUserWorkSpacae(ctx.from.id,'workSpace',ctx.update.callback_query.data);
  }
}

async function calculationText(ctx) {
  if (
    ctx.message.text.length == 16 &&
    (await dbctx.getState(ctx.from.id)) == "started"
  ) {
    try {
      let workSpace = await api.getWorkSpaces(ctx.message.text);
      let strId = "",
        strName = "";
      workSpace.data.forEach((element) => {
        strName += element.name + ",";
        strId += element.id + ",";
      });
      await dbctx.updateUser(ctx.from.id, "token", ctx.message.text);
      await view.okToken(ctx, strId, strName);
    } catch (error) {
      console.log(error);
      view.errorToken(ctx);
    }
  } else {
    ctx.deleteMessage();
  }
}
async function calculationCammandDeleteEntry(ctx) {
  let user=await dbctx.get(ctx.from.id);
  let idEntry=ctx.message.text;
  idEntry=idEntry.split('_');
  await api.deleteEntry(user.token,user.workid,idEntry[1])
  ctx.deleteMessage(ctx.message.message_id-1);
  view.sendMessage(ctx,'time entry deleted!');
}
module.exports = {
  calculationStart,
  calculationText,
  calculationCallbackQuery,
  calculationCammandDeleteEntry,
};
