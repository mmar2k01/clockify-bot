const { default: Axios } = require("axios");
const { time } = require("console");
const dbContext = require("./dbContext");
const datetime = new Date();
async function getWorkSpaces(token) {
    var result = await Axios.get("https://api.clockify.me/api/v1/workspaces", {
      headers: {
        "content-type": "application/json",
        "X-Api-Key": token,
      },
    });
    return result;
  }
  async function getUserId(workid,token){
    var result = await Axios.get(
        `https://api.clockify.me/api/v1/workspaces/${workid}/users`,
        {
          headers: {
            "content-type": "application/json",
            "X-Api-Key":  token,
          },
        }
      );
      return result.data[0].id;
  }

  async function getEntry(workid,userId,token){
    let times= await Axios.get(
        `https://api.clockify.me/api/v1//workspaces/${workid}/user/${userId}/time-entries`,
        {
          headers: {
            "content-type": "application/json",
            "X-Api-Key": token,
          },
        }
      );
      return times;
  }

  async function deleteEntry(token, work, identry) {
    console.log(work);
      await Axios.delete(
        `https://api.clockify.me/api/v1/workspaces/${work}/time-entries/${identry}`,
        {
          headers: {
            "content-type": "application/json",
            "X-Api-Key": token,
          },
        }
      );
  }


  async function salaryThisYear(id) {
    let user=await dbContext.get(id);
    try {
      var year, month, day;
      year = datetime.getFullYear();
      month = datetime.getMonth();
      day = datetime.getDate();
      month++;
      if (month<10) {
        month = "0" + month;
      }
      if (day<10 ) {
        day = "0" + day;
      }
      var result = await Axios.post(
        `https://reports.api.clockify.me/v1/workspaces/${user.workid}/reports/summary`,
        {
          dateRangeStart: `${year}-01-01T00:00:00.000Z`,
          dateRangeEnd: `${year}-${month}-${day}T23:59:59.999Z`,
          summaryFilter: {
            groups: ["USER", "PROJECT", "TIMEENTRY"],
          },
        },
        {
          headers: {
            "content-type": "application/json",
            "X-Api-Key": user.token,
          },
        }
      );
      return result.data.totals[0];
    } catch (error) {
      console.log(error);
    }
  }
  module.exports={getWorkSpaces,getUserId,getEntry,deleteEntry,salaryThisYear};