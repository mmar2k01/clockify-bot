const mongoose = require("mongoose");
const uri = "mongodb://localhost:27017/clockbot";
mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});
function connect() {
  const connectToDatabase = mongoose.connection;
  connectToDatabase.on("error", console.error.bind(console, "connection error:"));
  connectToDatabase.once("open", function () {
    console.log("connected");
  });
}
module.exports={connect};