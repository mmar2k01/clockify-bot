const connect = require("./db");
const Model = require("./user");
(async () => {
  await connect.connect();
})();

async function add(chatid, state,) {
  await Model.userModel.create({
    chatid: chatid,
    state: state,
  });
}

async function deleteUser(chatid) {
    await Model.userModel.deleteOne({
      chatid: chatid,
    });
  }

async function is(id) {
  let bool;
  await Model.userModel.find({ chatid: id }, async function (err, ok) {
    if (err) console.log("problem");
    if (ok == [] || ok == "" || ok == "") {
      bool = true;
    } else {
      bool = false;
    }
  });
  return bool;
}

async function updateUser(id, state, token, workid) {
  Model.userModel.updateOne(
    { chatid: id },
    { state: state, token: token, workid: workid },
    function (err, ok) {
      if (err) {
        console.error(err);
      }
    }
  );
}
async function updateUserWorkSpacae(id, state, workid) {
  Model.userModel.updateOne(
    { chatid: id },
    { state: state, workid: workid },
    function (err, ok) {
      if (err) {
        console.error(err);
      }
    }
  );
}
async function insertEmail(id, email, state) {
  Model.userModel.update(
    { chatid: id },
    { state: state, email: email },
    function (err, ok) {
      if (err) {
        console.error(err);
      }
    }
  );
}

async function getState(id) {
  let state;
  await Model.userModel.find({ chatid: id }, async function (err, ok) {
    if (err) console.log("problem");
    if ((await ok) == "") {
      bool = false;
    } else {
      state = ok[0].state;
    }
  });
  return state;
}

async function getWorkId(id) {
  var work;
  await Model.userModel.find({ chatid: id }, async function (err, ok) {
    if (err) console.log("problem");
    if ((await ok) == "") {
      bool = false;
    } else {
      work = ok[0].workid;
    }
  });
  return work;
}

async function get(id) {
  var person;
  var bool;
  await Model.userModel.find({ chatid: id }, async function (err, ok) {
    if (err) console.log("problem");
    if ((await ok) == "") {
      bool = false;
    } else {
      person = ok[0];
    }
  });
  return person;
}

module.exports = { add, is, get, getWorkId, getState, insertEmail, updateUser ,deleteUser,updateUserWorkSpacae};
