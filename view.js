const api = require("./api");

function wellcome(ctx) {
  ctx.reply(
    "wellcome to clockify api\nplease enter token clockify api from setting/profile :"
  );
  ctx.deleteMessage();
}
async function okToken(ctx, workSpacesId, workSpacesName) {
  let keyboard = [];
  let objsId = workSpacesId.split(",");
  let objsName = workSpacesName.split(",");
  await ctx.deleteMessage();
  await ctx.deleteMessage(ctx.message.message_id-1);
  for (var i = 0; i < objsName.length; i++) {
    keyboard.push({ text: objsName[i], callback_data: objsId[i] });
  }
  keyboard.pop();
  ctx.reply("please click for choose:", {
    reply_markup: {
      inline_keyboard: [keyboard],
    },
  });
}
async function okTokenAndGetWorkSpaces(ctx, workSpacesId, workSpacesName) {
  let keyboard = [];
  let objsId = workSpacesId.split(",");
  let objsName = workSpacesName.split(",");
  await ctx.deleteMessage();
  for (var i = 0; i < objsName.length; i++) {
    keyboard.push({ text: objsName[i], callback_data: objsId[i] });
  }
  keyboard.pop();
  ctx.reply("please click for choose:", {
    reply_markup: {
      inline_keyboard: [keyboard],
    },
  });
}
async function errorToken(ctx) {
  await ctx.reply("wrong token!\nplese enter again :");
}
async function salariReport(ctx,totalTime,totalBillableTime,entriesCount) {
  sendMessage(ctx,`total time :${parseInt(totalTime/3600)}\n total time with money:${parseInt(totalBillableTime/3600)}\n entriesCount :${entriesCount}`)
  
}

async function sendMessageWithReport(ctx,message) {
  await ctx.reply(message,{
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: "Salary this year 🤑", callback_data:'SalaryThisYear'
          },{
            text: "back", callback_data:'back'
          }
        ],
      ],
    }
  });
}

async function sendMessage(ctx,message) {
  await ctx.reply(message,{
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: "back", callback_data:'back'
          },
        ],
      ],
    }
  });
}
module.exports = { wellcome, errorToken, okTokenAndGetWorkSpaces,sendMessage,okToken,sendMessageWithReport,salariReport};
